import React from "react";
import fs from "fs-extra";
import path from "path";
import { getYmlConfig, logToConsole } from "@showroom/server/helper";
import { css_beautify, html_beautify, js_beautify } from "js-beautify";
import shortid from "shortid";
import { renderToString } from "react-dom/server";
import Icon from "./icon.html";
import config from "@root/config";

const Twig = require("twig");
Twig.cache(false);
Twig.extendFunction("uniqueId", function () {
  return Math.random().toString(36).substr(2, 9);
});

const innerButton = (name, icon) => {
  return icon ? (<Icon name={icon} title={icon} className="btn-icon icon-16"/>) : name;
};

export const rawMarkup = (filePath) => {
  let body = "";

  let extension = path.extname(filePath).replace(/[\#\?]+.*$/ig, '');

  if (extension === '.html') {
    filePath = filePath.replace('.html', '.twig')
  }

  body = fs.readFileSync(filePath, 'utf8');

  // https://github.com/beautify-web/js-beautify
  const jsBeautifyOptions = {
    "indent_size": 4,
    "indent_char": " ",
    "indent_with_tabs": false,
    "eol": "\n",
    "end_with_newline": false,
    "indent_level": 0,
    "preserve_newlines": true,
    "max_preserve_newlines": 10,
    "wrap_line_length": 100,
  };
  let highlightType = "";
  let contentString = body;
  switch (extension) {
  case '.js':
    highlightType = "javascript";
    contentString = js_beautify(body, jsBeautifyOptions);
    break;
  case '.css':
  case '.scss':
    highlightType = "css";
    contentString = css_beautify(body, jsBeautifyOptions);
    break;
  case '.twig':
      highlightType = "twig";
      contentString = body;
      break;
    break;
  case '.html':
  case '.htm':
  default:
    let content = Twig.twig({
      data: body,
      namespaces: {
        'phoenix': config.phoenixPath + '/components'
      }
    }).render();

    if (content) {
      body = content;
    }
    else {
      logToConsole("SHOWROOM-BUILDER", "(╯°□°）╯︵ ┻━┻", "Warning: Selector: " + filePath + "' not found.", true);
    }

    highlightType = "html";
    contentString = html_beautify(body, jsBeautifyOptions);
    break;
  }

  if (contentString.indexOf('\n', 0) === -1) {
    contentString += '\n\n';
  }

  // console.log(contentString)
  return {
    contentString,
    highlightType, // corresponds to the highlight.js highlighting class (eg. "javascript", "csharp", …)
  };
};

const mergeObj = (searchObj, masterObj) => {
  if (typeof searchObj !== 'object' && typeof masterObj === 'object') {
    searchObj = masterObj;
  }
  else {
    for (const searchKey in searchObj) {
      if (masterObj[searchKey]) {
        if (typeof masterObj[searchKey] !== 'object' || typeof searchObj[searchKey] !== 'object') {
          searchObj[searchKey] = masterObj[searchKey]
        }
        else {
          searchObj[searchKey] = mergeObj(searchObj[searchKey], masterObj[searchKey]);
        }
      }
    }
    for (const masterKey in masterObj) {
      searchObj[masterKey] = masterObj[masterKey];
    }
  }
  return searchObj;
};

const ContentRack = ({ siteObject, name = null, fields = null }) => {
  const filePath = siteObject.filePath;
  const folderPath = filePath.substr(0, filePath.lastIndexOf("/") + 1);
  if (fields) {
    fields = JSON.parse('{' + fields + '}');
  }

  if (name) {
    const ymlFile = folderPath + 'content-rack.yml';

    let ymlConfig = getYmlConfig(ymlFile);

    if (ymlConfig[name]) {
      ymlConfig = ymlConfig[name]

      if (fields && ymlConfig.fields) {
        fields = mergeObj(ymlConfig.fields, fields);
      }
    }
  }

  let btnTemplates = [];
  let contentTemplates = [];
  const rackId = shortid.generate();
  for (const buttonName in fields) {
    if (!fields[buttonName].src) {
      continue;
    }
    const id = shortid.generate();
    let href = fields[buttonName].src;
    let btnTemplate = null;
    let contentTemplate = null;
    switch (fields[buttonName].type) {
    case 'link':
      btnTemplate = (
        <a
          key={shortid.generate()}
          className="btn btn-primary PhoenixFramework__JSIndicator"
          target="_blank"
          href={href}
        >
          {innerButton(buttonName, fields[buttonName].icon)}
        </a>
      );
      break;
    case 'download':
      btnTemplate = (
        <a
          key={shortid.generate()}
          className="btn btn-primary PhoenixFramework__JSIndicator"
          href={href}
        >
          {innerButton(buttonName, fields[buttonName].icon)}
        </a>
      );
      break;
    case 'content':
    default:
      btnTemplate = (
        <label key={shortid.generate()} htmlFor={id} type="button" data-controller="content-rack/content/label" className="btn btn-primary PhoenixFramework__JSIndicator">{innerButton(buttonName, fields[buttonName].icon)}</label>
      );
      let srcFilePath = folderPath + fields[buttonName].src;

      let filePath = srcFilePath
      if (path.extname(srcFilePath) === '.html') {
        filePath = srcFilePath.replace('.html', '.twig')
      }

      if (fs.existsSync(filePath)) {
        const { contentString, highlightType } = rawMarkup(srcFilePath);

        contentTemplate = (
          <React.Fragment key={buttonName}>
            <input
              key={shortid.generate()}
              name={rackId}
              defaultChecked={fields[buttonName].active == true}
              type="radio"
              className="PhoenixFramework__CodeSectionCheckbox"
              id={id}
            />
            <div key={shortid.generate()} className="PhoenixFramework__CodeSection">
                                <pre className="PhoenixFramework__Pre">
                                    <code className={highlightType + " PhoenixFramework__TextArea"}>
                                        {
                                          contentString
                                        }
                                    </code>
                                </pre>
            </div>
          </React.Fragment>
        );
        contentTemplates.push(contentTemplate);
      }
      else {
        logToConsole("SHOWROOM-BUILDER", "(╯°□°）╯︵ ┻━┻", "Warning: Example file: '" + srcFilePath + "' not found.", true);
      }
    }
    btnTemplates.push(btnTemplate);
  }

  return (
    <section className="PhoenixFramework__Section">
      <h3 className="PhoenixFramework__SectionHeadline">
        <div className="btn-group btn-group-sm float-right" role="group" aria-label="Component options">
          {btnTemplates.map(btnTemplate => {
            return btnTemplate;
          })}
        </div>
      </h3>
      {contentTemplates.map(contentTemplate => {
        return contentTemplate;
      })}
    </section>
  );
};

export default ContentRack;
