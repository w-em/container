import React from "react";
import fs from "fs-extra";
import path from "path";
import Iframe from "@showroom/server/components/iframe";

const renderImage = (imgSrc, title, alt, siteObject) => {
    const assetStartPath = "assets/";
    let imgSrcSet = imgSrc;
    if (imgSrc.substr (0, assetStartPath.length) === assetStartPath) {
        imgSrcSet = imgSrc;

        //Check if img-source has a density-suffix (e.g. @1x) and add each density-image to sourceset
        const densityRegex = /^(.*@)(.*)(x\.)(.*)/ig;
        densityRegex.lastIndex = 0; //reset RegExp
        if (densityRegex.test (imgSrc)) {
            imgSrcSet = "";
            densityRegex.lastIndex = 0; //reset RegExp
            const srcSetGroups = densityRegex.exec (imgSrc);

            let fileExists = true;
            let densityNr = 1;
            while (fileExists) {
                const srcSetUrl = srcSetGroups [1] + densityNr + srcSetGroups [3] + srcSetGroups [4];
                const filePath = path.resolve (path.dirname (siteObject.filePath), srcSetUrl);

                //Check if the file exists
                if (fs.existsSync (filePath)) {
                    imgSrcSet += ", " + srcSetUrl + " " + densityNr + "x";
                } else {
                    imgSrcSet = imgSrcSet.substr (2)
                    fileExists = false;
                }
                densityNr++;
            }
        }
    }
    return (
        <a href={imgSrc} target="_blank">
            <img
                className="img-fluid mw-100"
                title={title}
                alt={alt}
                src={imgSrc}
                srcSet={imgSrcSet}
            />
        </a>
    );
};

const renderHtml = (src, title, alt, siteObject) => {
    return (
        <Iframe
            src={src}
            alt={alt}
            title={title}
        />
    );
}

const renderVideo = (src, title, alt, siteObject) => {
    return (
        <video
            className="embed-responsive mw-100"
            controls
            src={src}
            title={title}
            alt={alt}
        >
            Sorry, your browser doesn't support embedded videos,
            but don't worry, you can <a href={src}>download it</a>
            and watch it with your favorite video player!
        </video>
    );
}

const ShowroomExample = (...props) => {
    const src = props [0].src;

    // returns the suffix
    const fileExtension = path.extname (src).replace (/[\#\?]+.*$/ig, '');

    switch (fileExtension) {
        case '.png':
        case '.jpg':
        case '.jpeg':
        case '.gif':
            return renderImage (
                src,
                props [0].title,
                props [0].alt,
                props [0].siteObject
            );

        case '.html':
        case '.htm':
            return renderHtml (
                src,
                props [0].title,
                props [0].alt,
                props [0].siteObject
            );

        case '.mp4':
        case '.webm':
        case '.ogv':
            return renderVideo (
                src,
                props [0].title,
                props [0].alt,
                props [0].siteObject
            );
        default:
            return null;
    }
}

export default ShowroomExample;
