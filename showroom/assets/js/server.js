import hljs from 'highlight.js';
import "../../../phoenix/js/themeDOM";
import "iframe-resizer/js/iframeResizer.js";

/*
    Add highlighting for example html snippets
*/
hljs.initHighlightingOnLoad();

/*
    Resize Example-Iframes when content changes
*/
document.addEventListener('DOMContentLoaded', () => iFrameResize({
    scrolling: "omit",
}, '*[data-controller="theme/example/iframe"]'));

/*
    Change Active status of anchor links on click or on-load
*/

document.addEventListener ('DOMContentLoaded', () => {
    //Extracting anchor from url/link
    let getAnchor = (url) => {
        let split = url.split('#');
        return split.length > 1 ? split [1] : null;
    }

    //Extract anchor informations
    let anchor = getAnchor (document.URL);
    let anchorLinks = document.querySelectorAll ('a[data-controller="phoenix/sidenav/anchor/link"]');

    //set anchor to active state
    let setAnchorActive = (anchorLink) => {
        //Remove active state all Anchor Links with active
        anchorLinks.forEach (anchorLink => {
            if (anchorLink.classList.contains ('active')) {
                anchorLink.classList.remove ('active');
            }
        });
        anchorLink.classList.add ('active');
    }

    anchorLinks.forEach (anchorLink => {
        //Set anchor active, if one is set in url
        if (getAnchor (anchorLink.href) === anchor) {
            setAnchorActive (anchorLink);
        }

        //Set active state for clicked anchor link
        anchorLink.addEventListener ('click', () => {
            setAnchorActive (anchorLink);
        });
    });
});

/*
    Header
*/

document.addEventListener('DOMContentLoaded', () => {
    const closeMobileMenuBreakpoint = 960;
    const MenuOpenClass = 'is-active';

    const headerOverlayBackground = document.querySelector('*[data-controller="header/overlay/background"]');
    const burgerMenu = document.querySelector('*[data-controller="header/burgermenu"]');
    const SideNav = document.querySelector('*[data-controller="side/nav"]');
    const productMenu = document.querySelector('*[data-controller="header/product/menu"]');
    const productNav = document.querySelector('*[data-controller="header/product/nav"]');
    const sectionMenu = document.querySelector('*[data-controller="header/section/menu"]');
    const sectionNav = document.querySelector('*[data-controller="header/section/nav"]');

    if (!burgerMenu || !SideNav || !productMenu) {
        return;
    }

    const closeAllMenus = () => {
        burgerMenu.classList.remove(MenuOpenClass);
        burgerMenu.setAttribute('aria-pressed', false);
        SideNav.setAttribute('aria-expanded', false);
        productMenu.setAttribute('aria-pressed', false);
        productNav.setAttribute('aria-expanded', false);
        sectionMenu.setAttribute('aria-pressed', false);
        sectionNav.setAttribute('aria-expanded', false);
    };

    headerOverlayBackground.addEventListener('click', (e) => {
        closeAllMenus();
    });

    const toggleNav = (menu, nav, menuClass = '') => {
        menu.addEventListener ('click', (e) => {
            if (menu.hasAttribute('aria-pressed') && menu.getAttribute('aria-pressed') === 'true') {
                menu.setAttribute('aria-pressed', false);
                nav.setAttribute('aria-expanded', false);
                if(menuClass) {
                    menu.classList.remove(menuClass);
                }
            } else {
                closeAllMenus();
                menu.setAttribute('aria-pressed', true);
                nav.setAttribute('aria-expanded', true);
                if(menuClass) {
                    menu.classList.add(menuClass);
                }
            }
        });
    };

    toggleNav(burgerMenu, SideNav, MenuOpenClass);
    toggleNav(productMenu, productNav);
    toggleNav(sectionMenu, sectionNav);

    let isMobile = null;
    //Add Event-Listener to close mobile-menu on resize, when on breakpoint >md
    window.resizeThrottled (() => {
        if (closeMobileMenuBreakpoint <= window.innerWidth && isMobile === true) {
            closeAllMenus();
            SideNav.removeAttribute('aria-expanded');
            productNav.removeAttribute('aria-expanded');
            sectionNav.removeAttribute('aria-expanded');
            isMobile = false;
        } else if (isMobile === false) {
            closeAllMenus();
            isMobile = true;
        }
    }, 200);

    if ( !(closeMobileMenuBreakpoint <= window.innerWidth) ) {
        closeAllMenus();
        isMobile = true;
    } else {
        isMobile = false;
    }
});

/*
    Something
*/

document.addEventListener('DOMContentLoaded', ()=>{
    let phoenixKeys = [];
    const phoenixName = "phoenix";
    const phoenixSequence = phoenixName.split('').toString();
    const phoenixSequenceLength = phoenixName.split('').length;
    const phoenixQuotes = [
        "Eat phoenix and be happy",
        "Life's too short to say no to phoenix!",
        "phoenix doesn't ask silly questions. phoenix understands.",
        "All you need is love and phoenix",
        "Sometimes you just need phoenix.",
        "phoenix is the answer, who cares what the question is.",
        "Baked with love.",
        "I decorate phoenix, what's your superpower?",
        "Lets celebrate with phoenix",
        "I don't always make phoenix... Oh wait, yes I do.",
        "In this uncertain world, a good piece of phoenix, is at least, an undeniable pleasure.",
        "Love is like a good phoenix; you never know when it's coming, but you'd better eat it when it does!",
        "Hois! Hois! Hois! Hois! Hois!",
        "Lok'tar Ogar",
        "Ich empfehle mich. (I recommend myself.)",
        "Here is my pamphlet.",
        "Beware of a wild zapfdingbats in tall grass.",
        "I need nearly no space, I am a WebEx.",
        "You can find it at the end of the backlog.",
        "I'm Mr. M. Look at me!",
        "I have to fulfill my purpose, so I can go away."
    ];
    const phoenixQuotesLength = phoenixQuotes.length;

    document.onkeyup = function(e) {
        phoenixKeys.push( e.key );
        if ( phoenixKeys.toString().indexOf( phoenixSequence ) >= 0 ) {
            let oldElems = document.querySelectorAll('body > .something');
            let phoenixQuoteKey = Math.floor(Math.random() * Math.floor(phoenixQuotesLength));
            let animationDuration = ( ((phoenixQuotes[phoenixQuoteKey].length - (phoenixQuotes[phoenixQuoteKey].length % 10)) / 10 ) * 500 ) + 1000;
            let newElemBobble = document.createElement('div');
            let newElemWrapper = document.createElement('div');
            let newElem = document.createElement('div');

            if( oldElems ) {
                oldElems.forEach(function(oldElem, index) {
                    oldElem.parentNode.removeChild(oldElem);
                });
            }

            newElemBobble.classList.add('bubble');
            newElemBobble.innerHTML = phoenixQuotes[phoenixQuoteKey];

            newElemWrapper.classList.add('bubble-wrapper');
            newElemWrapper.appendChild(newElemBobble);

            newElem.classList.add('something', 'now');
            newElem.style.animationDuration = animationDuration.toString() + "ms";
            newElem.appendChild(newElemWrapper);
            document.body.appendChild(newElem);

            phoenixKeys = [];
            e.stopPropagation();
        }
        if ( phoenixKeys.length >= phoenixSequenceLength ) {
            phoenixKeys.shift();
        }
    };
});


/*
    Keep scroll-position on page-change
*/
document.addEventListener ("DOMContentLoaded", () => {

    const sideNavEl = document.querySelector ('[data-controller="side/nav/container"]');
    if (!sideNavEl) {
        return;
    }
    const activeLink = sideNavEl.querySelector (".active");
    if (!activeLink) {
        return;
    }

    const sideNavElOffsetHeight = sideNavEl.offsetHeight;
    const activeLinkParentHeight = activeLink.parentElement.offsetHeight;
    const centerMenuValues =  - (sideNavElOffsetHeight / 2) + (activeLinkParentHeight / 2);

    // calculate new scrollTop position of menu to center
    if (sideNavElOffsetHeight > activeLinkParentHeight) {
        sideNavEl.scrollTop = activeLink.offsetTop + centerMenuValues;
    } else {
        sideNavEl.scrollTop = activeLink.offsetTop;
    }
});


/*
    Content-Rack (labels/radio) uncheck radio on repeated click on checked radio input
*/
document.addEventListener ("DOMContentLoaded", () => {
    const contentRackLabels = document.querySelectorAll ('[data-controller="content-rack/content/label"]');

    for (const contentRackLabel of contentRackLabels) {
        const contentRackInput = document.getElementById (contentRackLabel.getAttribute ("for"));
        contentRackLabel.addEventListener ("click", () => {
            if (contentRackInput.checked) {
                setTimeout (() => {
                    contentRackInput.checked = false;
                }, 17);
            }
        });
    }
});




/*
    Fix wrong vh on mobile devices (iOS + android) with overlapping menu bars
*/
document.addEventListener ('DOMContentLoaded', () => {

    const elements = document.querySelectorAll ('[data-controller="helpers/fullviewheight"]');
    if (elements.length <= 0) {
        return;
    }

    // set transitions for this element
    for (const element of elements) {
        element.style.transition = "height .2s ease";
    }

    const resizeViewport = () => {
        for (const element of elements) {
            element.style.height = window.innerHeight + "px";
        }
    };

    window.resizeThrottled (() => {
        resizeViewport ();
    });

    // initially set correct view-port height
    resizeViewport ();
});
