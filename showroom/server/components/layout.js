import React from "react";
import Header from "./header";
import Sidenav from "./Sidenav";

const Layout = ( {
                     routes,
                     siteObject,
                     children = null,
                 } ) => (
  <React.Fragment>
      <div className={"PhoenixFramework__Layout container-responsive"}>
          <Header
            routes={routes}
            siteObject={siteObject}
          />
          <div className="row">
            <Sidenav
              routes={routes}
              siteObject={siteObject}
            />
              <div className="PhoenixFramework__Content">
                  <main className="PhoenixFramework__Main">
                      {children}
                  </main>
              </div>
          </div>
      </div>
      {/* // Hide CI-Selector */}
      {/* <CiSelector /> */}
  </React.Fragment>
);

export default Layout;
