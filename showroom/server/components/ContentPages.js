import React from "react";
import Helmet from "react-helmet";
import config from "@root/config";

/*
    Imports all content pages and gathers them to one export.
 */

import Home from "./home";

export default [
    {
        component: Home,
        filePath: "index.html",
        title: "Welcome to Phoenix",
        head: ({}) => (
            <Helmet>
                <link rel="stylesheet" type="text/css" href={config.assetPath + "css/home.css"} />
            </Helmet>
        ),
        foot: () => (
            <React.Fragment>
                <script src={config.assetPath + "js/server.js"}></script>
            </React.Fragment>
        ),
    },
];
