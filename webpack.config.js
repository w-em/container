const glob = require("glob");
const path = require("path");
const fs = require("fs");
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const FileManagerPlugin = require('filemanager-webpack-plugin');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const packageJson = require("./package.json");
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack');

const _moduleAliases = packageJson._moduleAliases;
let absolutedAliases = {};

const uniqueFileNames = (pathArray, ext) => {
  let usedNames = [];
  let iconArray = [];
  for (let iconPath of pathArray.reverse()) {
    const iconName = path.basename(iconPath, ext);
    if (!usedNames.includes(iconName)) {
      usedNames.push(iconName);
      iconArray.push(iconPath);
    }
  }
  return iconArray;
};

for (let alias in _moduleAliases) {
  if (_moduleAliases.hasOwnProperty(alias)) {
    absolutedAliases[alias] = path.resolve(__dirname, _moduleAliases[alias])
  }
}

const walkDirectory = (dir) => {
  let results = [];
  const list = fs.readdirSync(dir);
  list.forEach(file => {
    file = `${dir}/${file}`;
    const stat = fs.statSync(file);
    if (stat && stat.isDirectory() && path.basename(file).indexOf('_') !== 0) {
      /* Recurse into a subdirectory */
      results = results.concat(walkDirectory(file));
    }
    else if (
      stat &&
      !stat.isDirectory() &&
      path.extname(file) === '.twig' &&
      path.basename(file).indexOf('_') !== 0
    ) {
      /* Is a file */
      results.push(file);
    }
  });
  return results;
}

//Method to extract all files of sourcePath and compile them to distPath (with minified version and exclusion of files)

const getEntryFiles = (sourcePath, distPath, minified = true) => {
  let entryFiles = {};
  for (let file of glob.sync(sourcePath)) {
    entryFiles [distPath + path.parse(file).name] = file;
    if (minified) {
      entryFiles [distPath + path.parse(file).name + ".min"] = file;
    }
  }
  return entryFiles;
};

// - - - - - - - - - - - - - - - -
// Starting with Webpack functions
// - - - - - - - - - - - - - - - -
// generate object of all files to be processed
const entry = {
  // theme documentation assets
  ...getEntryFiles("./src/js/*.js", "/assets/js/", false),
  ...getEntryFiles("./src/styles/!(_)*.scss", "/assets/css/", false),
  ...getEntryFiles("./showroom/assets/styles/!(_)*.scss", "docs/assets/css/", false),
  ...getEntryFiles("./showroom/assets/js/*.js", "docs/assets/js/", false),
};

const twigPlugins = () => {
  let result = []

  //start looking in the main twig folder
  let files = walkDirectory('./src/');
  files.map((file) => {

    if (file.indexOf('docs') > -1 && file.indexOf('pages') > -1) {
      let dirs = file.substr(file.indexOf('docs')).split('/')
      let distPath = './docs'

      dirs = dirs.slice(dirs.indexOf('pages') + 1)
      dirs.pop()

      if (dirs.length > 0) {
        distPath += '/' + dirs.join('/')
      }

      result.push(
        new HtmlWebpackPlugin({
          filename: distPath + '/' + path.parse(file).name + '.html',
          template: path.resolve(__dirname, file),
          hash: true,
          inject: true,
          meta: {
            title: path.parse(file).name
          },
          publicPath: './',
          title: path.parse(file).name
        })
      )
    }
    else if (file.indexOf('pages') > -1) {
      result.push(
        new HtmlWebpackPlugin({
          filename: './' + path.parse(file).name + '.html',
          template: path.resolve(__dirname, file),
          hash: true,
          inject: true,
          meta: {
            title: path.parse(file).name
          },
          publicPath: './',
          title: path.parse(file).name
        })
      )
    }
  });

  return result
}

// resolve all aliases in the entry files (eg @develop)
const resolve = {
  modules: [
    path.resolve("./"),
    "node_modules"
  ],
  alias: absolutedAliases
};

// main processing of all entry files
// configuration of scss processor and bable js processor (ES6 -> ES5)
const generateFiles = {
  rules: [
    {
      test: /\.html$/,
      use: [
        'raw-loader',
        {
          loader: 'html-loader',
          options: {
            data: {},
          },
        },
      ],
    },
    {
     test: /\.twig$/,
     use: [
       'raw-loader',
       {
         loader: 'twig-html-loader',
         options: {
           debug: false,
           namespaces: {
             'phoenix': 'phoenix/components',
             'layouts': 'src/templates/layouts',
             'components': 'src/templates/components',
           },
           data: (context) => {
             return {
               context: context,
               file: path.parse(context.resourcePath)
             }
           },
           functions: {
             uniqueId () {
               return Math.random().toString(36).substr(2, 9);
             }
           }
         },
       },
     ],
    },
    {
      test: /\.scss$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader
        },
        {
          loader: "css-loader", // translates CSS into CommonJS
          options: {
            url: false
          }
        },
        {
          loader: "postcss-loader",
          options: {
            plugins: () => [
              require('postcss-inline-svg')({
                removeFill: true,
                xmlns: true,
                path: process.cwd() + '/'
              }),
              require('autoprefixer'),
            ]
          }
        },
        {
          loader: "sass-loader", // compiles Sass to CSS, using Dart-Sass by default
          options: {
            implementation: require('sass'),
          },
        }
      ],
    },
    {
      test: /\.(js)$/,
      exclude: /node_modules/,
      use: {
        loader: "babel-loader"
      }
    }
  ],
};

// post precessing of all entry files after main processing
const optimization = {
  minimize: true,
  minimizer: [
    new UglifyJsPlugin({
      include: /\.min\.js$/,
      sourceMap: true,
      uglifyOptions: {
        output: {
          comments: false,
        },
      }
    }),
    new OptimizeCSSAssetsPlugin({
      assetNameRegExp: /\.min\.css$/g,
    }),
  ]
};

// start tools if processing of all entry files is done
const plugins = [
  new FriendlyErrorsWebpackPlugin({
    clearConsole: true,
  }),
  new MiniCssExtractPlugin(),
  // Theme icons
  new SVGSpritemapPlugin(
    uniqueFileNames([
      //Sort the icons by importance! (the last line overwrites all previous ones, if the name equals)
      ...glob.sync('./phoenix/assets/icons/!(_)*.svg'),
      ...glob.sync('./phoenix/assets/icons/!(_)**/!(_)*.svg'),
      ...glob.sync('./src/assets/icons/!(_)*.svg'),
      ...glob.sync('./src/assets/icons/!(_)**/!(_)*.svg'),
    ], '.svg'), {
      output: {
        filename: './assets/images/icon__sprite.svg',
        svgo: true,
      },
      sprite: {
        prefix: false,
      }
    }),
  new FileManagerPlugin({
    onEnd: {
      copy: [
        { source: './src/assets/!(icons)**/*', destination: path.resolve(__dirname, './dist/assets') },
        { source: './showroom/assets/!(styles|js)/*', destination: path.resolve(__dirname, './dist/docs/assets') }
      ],
    }
  }),
  new FixStyleOnlyEntriesPlugin(),
  // new webpack.HotModuleReplacementPlugin({multistep: true})
].concat(twigPlugins());

module.exports = {
  mode: 'development',
  output: {
    publicPath: './',
    path: path.resolve(__dirname, './dist/'),
  },
  context: path.join(__dirname, "./"),
  entry: entry,
  resolve: resolve,
  module: generateFiles,
  optimization: optimization,
  plugins,
  stats: 'errors-warnings', // 'errors-warnings', 'verbose'
  devtool: "cheap-source-map", // prevent babel from using "eval" in javascript (https://webpack.js.org/configuration/devtool/)
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
};
