import config from "@root/config";

/*
    Global HTML Template Wrapper
    All routes are rendered inside of this template.
 */
export const htmlTemplate = (reactDom, helmetData) =>
  `
        <!DOCTYPE html>
        <html lang="en-GB">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            ${helmetData.title.toString()}
            ${helmetData.meta.toString()}
            <title>PHOENIX</title>
            <link rel="stylesheet" type="text/css" href="${config.assetPath}css/theme.css">
            <link rel="stylesheet" type="text/css" href="${config.assetServerPath}css/docs.css">
            <link rel="apple-touch-icon" sizes="57x57" href="${config.assetServerPath}favicon/57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="${config.assetServerPath}favicon/60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="${config.assetServerPath}favicon/72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="${config.assetServerPath}favicon/76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="${config.assetServerPath}favicon/114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="${config.assetServerPath}favicon/120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="${config.assetServerPath}favicon/144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="${config.assetServerPath}favicon/152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="${config.assetServerPath}favicon/180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="${config.assetServerPath}favicon/192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="${config.assetServerPath}favicon/32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="${config.assetServerPath}favicon/96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="${config.assetServerPath}favicon/16x16.png">
            <link rel="manifest" href="${config.assetServerPath}favicon/manifest.json">
    
            <script src="//code.jquery.com/jquery-3.3.1.slim.min.js"></script> <!-- Only for Dropdowns in Showroom -->
            <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script> <!-- Only for Dropdowns in Showroom -->
            <script src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> <!-- Only for Dropdowns in Showroom -->
            <script src="${config.assetServerPath}js/server.js"></script>
        </head>

        <body>
            <div id="app">${reactDom}</div>
            <div class="break">
                <div class="little-m"></div>
            </div>
            <script src="${config.assetPath}js/phoenix.js"></script>
        </body>
        </html>
    `;

/*
    HTML Template Wrapper for Examples
    All examples are rendered inside of this template.
 */
export const htmlTemplateExample = (reactDom, helmetData) =>
  `
        <!DOCTYPE html>
        <html lang="en-GB">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            ${helmetData.title.toString()}
            ${helmetData.meta.toString()}
            <title>PHOENIX</title>
            <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
            <link rel="stylesheet" type="text/css" href="${config.assetPath}css/theme.css">
            <link rel="apple-touch-icon" sizes="57x57" href="${config.assetServerPath}favicon/57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="${config.assetServerPath}favicon/60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="${config.assetServerPath}favicon/72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="${config.assetServerPath}favicon/76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="${config.assetServerPath}favicon/114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="${config.assetServerPath}favicon/120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="${config.assetServerPath}favicon/144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="${config.assetServerPath}favicon/152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="${config.assetServerPath}favicon/180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="${config.assetServerPath}favicon/192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="${config.assetServerPath}favicon/32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="${config.assetServerPath}favicon/96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="${config.assetServerPath}favicon/16x16.png">
            <link rel="manifest" href="${config.assetServerPath}favicon/manifest.json">
        </head>

        <body>
            <div id="app">${reactDom}</div>
            <script src="${config.assetPath}js/phoenix.js"></script>
            <script src="${config.assetServerPath}js/example.js"></script>
        </body>
        </html>
    `;

/*
    HomeBanner HTML Template Wrapper
 */
export const htmlTemplateHome = (reactDom, helmetData) =>
  `
    <!DOCTYPE html>
    <html lang="en-GB">
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${helmetData.title.toString()}
        ${helmetData.meta.toString()}
        <title>PHOENIX</title>
        <link rel="apple-touch-icon" sizes="57x57" href="${config.assetServerPath}favicon/57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="${config.assetServerPath}favicon/60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="${config.assetServerPath}favicon/72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="${config.assetServerPath}favicon/76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="${config.assetServerPath}favicon/114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="${config.assetServerPath}favicon/120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="${config.assetServerPath}favicon/144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="${config.assetServerPath}favicon/152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="${config.assetServerPath}favicon/180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="${config.assetServerPath}favicon/192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="${config.assetServerPath}favicon/32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="${config.assetServerPath}favicon/96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="${config.assetServerPath}favicon/16x16.png">
            <link rel="manifest" href="${config.assetServerPath}favicon/manifest.json">
        <link rel="stylesheet" type="text/css" href="${config.assetServerPath}css/home.css">
    </head>

    <body>
        <div id="app">${reactDom}</div>
        <script src="${config.assetPath}js/phoenix.js"></script>
    </body>
    </html>
`;
