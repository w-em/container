import config from "@root/config";
import fs from "fs-extra";
import path from "path";
import { JSDOM } from "jsdom";
import { secureFilePath } from "@showroom/server/helper";

const Twig = require("twig");
Twig.cache(false);
Twig.extendFunction("uniqueId", function () {
    return Math.random().toString(36).substr(2, 9);
});

const prepareHtmlExample = async (htmlFilePath, destPath, documentationRootPath) => {

    const htmlContent = await fs.readFile (htmlFilePath, {
        encoding: 'utf8'
    });
    let content = ''
    content = Twig.twig({
        data: htmlContent,
        namespaces: {
            'phoenix': config.phoenixPath + '/components'
        }
    }).render();

    // Get the needed NodeElements
    const exampleDOM = new JSDOM (content);
    const headTag = exampleDOM.window.document.querySelector("html > head");
    const bodyTag = exampleDOM.window.document.querySelector ("html > body");

    // Read-base tag, if exists
    const baseTag = headTag.querySelector("base");
    let baseHref = "./";
    if (baseTag) {
        baseHref = baseTag.getAttribute ("href");
    }

    // Calculate showroom-assets-path
    const showroomAssetPath =
        path.relative (
            path.resolve (
                path.dirname (htmlFilePath),
                baseHref
            ),
            documentationRootPath
        ) +
        "/assets/";


    // Prepare titlte-tag (HEADER)
    const headerSuffix = " | PHOENIX";


    // Ensure the titleTag exists
    let titleTag = headTag.querySelector("title");
    if (!titleTag) {
        titleTag = exampleDOM.window.document.createElement ("title");
        titleTag.textContent = path.basename (htmlFilePath, path.extname (htmlFilePath));
        headTag.prepend (titleTag);
    }
    // Append header-suffix to title
    if (titleTag.textContent.slice (- (headerSuffix.length)) !== headerSuffix) {
        titleTag.textContent += headerSuffix;
    }

    // Add component-example css
    const componentExampleLinkTag = exampleDOM.window.document.createElement ("link");
    componentExampleLinkTag.rel = "stylesheet";
    componentExampleLinkTag.type = "text/css";
    componentExampleLinkTag.href = showroomAssetPath + "css/component-examples.css";


    // Prepend all the important tags to the header
    headTag.prepend (
        componentExampleLinkTag
    );

    // Add example script tag
    const exampleScriptTag = exampleDOM.window.document.createElement ("script");
    exampleScriptTag.src = showroomAssetPath + "js/example.js";
    bodyTag.appendChild (exampleScriptTag);


    // Save example to desitnation path, to not modify the src-example file
    destPath = secureFilePath (destPath);
    await fs.ensureFile (destPath);
    await fs.writeFile (destPath, exampleDOM.serialize (), {
        encoding: "utf8"
    });
}

export default prepareHtmlExample;
