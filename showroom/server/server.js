import "module-alias/register";
import express from "express";
import helmet from "helmet";
import path from "path";
import Helmet from "react-helmet";
import React from "react";
import { renderToString, renderToStaticMarkup } from "react-dom/server";
import Home from "@showroom/server/components/home";
import Layout from "@showroom/server/components/layout";
import MarkdownPage from "@showroom/server/components/MarkdownPage";

import config from "@root/config";
import { getRoutes } from "./helper";
import { htmlTemplate, htmlTemplateExample, htmlTemplateHome } from "@showroom/server/templates";
import fs from "fs-extra";


const Twig = require("twig");
Twig.cache(false);
Twig.extendFunction("uniqueId", function () {
  return Math.random().toString(36).substr(2, 9);
});

const app = express();

// This section is optional and used to configure twig.

app.set("twig options", {
  allow_async: true, // Allow asynchronous compiling
  strict_variables: false,
  namespaces: {
    'phoenix': config.phoenixPath + '/components',
  }
});

const twigLayoutPath = config.showroomPath + '/server/layouts';

// Read the routes avaliable
const routes = getRoutes(config.symlinkDocumentationPath);

// Add Strict-Transport-Security header to force encrypted transfer of data
app.use(helmet());

app.use("/assets/", express.static(path.resolve(__dirname, "../../dist/assets")));
app.use("/assets-server/", express.static(path.resolve(__dirname, "../../dist/docs/assets")));
app.use("/assets-server/fonts", express.static(path.resolve(__dirname, "../../dist/assets/fonts")));

// Middleware to check for old-showroom links and redirect to new link-structure if possible
app.use((request, response, next) => {
    const relevantFileExtension = ".html";
    const queries = Object.keys(request.query).length > 0 ? "?" + new URLSearchParams(request.query).toString() : "";
    const urlPath = request.path;
    let urlPathParts = urlPath.split("/");

    let idx = 1;
    const requestSection = urlPathParts[idx] ? decodeURIComponent(urlPathParts[idx]).toLowerCase() : undefined;
    const requestSite = urlPathParts[idx + 1] ? decodeURIComponent(urlPathParts[idx + 1]).replace(relevantFileExtension, '') : undefined;
    const requestExample = urlPathParts[idx + 2] ? decodeURIComponent(urlPathParts[idx + 2]) : undefined;
    const requestExamplePage = urlPathParts[idx + 3] ? decodeURIComponent(urlPathParts[idx + 3]) : undefined;

    // console.log('routes', routes)
/*
    console.log('requestSection', requestSection)
    console.log('requestSite', requestSite)
    console.log('requestExample', requestExample)
    console.log('urlPath', urlPath)
*/
    if (requestSection && routes [requestSection]) {
      const siteObject = routes [requestSection].find(route => route.key === requestSite);
      if (requestExample === 'examples' && requestExamplePage) {

        let twigFile = config.symlinkDocumentationPath + urlPath

        twigFile = twigFile.replace('html', 'twig')
        let content = Twig.twig({
          data: fs.readFileSync(twigFile, 'utf8'),
          namespaces: {
            'phoenix': config.phoenixPath + '/components'
          }
        }).render();

        response.writeHead(200, { "Content-Type": "text/html" });
        return response.end(htmlTemplateExample(content, Helmet.renderStatic()));
      }
      else {
        config.setActiveMenuSection(siteObject);
        //Render Showroom-page, if no example subpath is given
        const jsx = (
          <Layout routes={routes} siteObject={siteObject}>
            <MarkdownPage
              routes={routes}
              siteObject={siteObject}
            />
          </Layout>
        );
        response.writeHead(200, { "Content-Type": "text/html" });
        return response.end(htmlTemplate(renderToStaticMarkup(jsx), Helmet.renderStatic()));
      }
    }
    else {
      const jsx = (
        <Home routes={routes}/>
      );
      response.writeHead(200, { "Content-Type": "text/html" });
      return response.end(htmlTemplateHome(renderToString(jsx), Helmet.renderStatic()));
    }

    return next();
  }
);

// Add 404-Error handling
app.use((req, res, next) => {
  console.error("ERROR", "SHOWROOM-SERVER", req.originalUrl);
  // res.redirect ("/");
  next();
});

app.listen(config.developementServerPort);
console.log("Server is listening on port " + config.developementServerPort);
