import React from "react";
import fs from "fs-extra";
import Markdown from "markdown-to-jsx";
import config from "@root/config";
import { logToConsole, readMarkupFile, secureFilePath } from "@showroom/server/helper";
import ContentRack from "@showroom/server/components/contentRack";
import ShowroomExample from "@showroom/server/components/showroomExample";
//import Iframe from "@Server/Components/Iframe.html";
import Table from "@showroom/server/components/table.html";
import path from "path";

const adjustLink = (href, routes, siteObject) => {
  //Check if this is an external link
  if ((new RegExp('^(?:[a-z]+:)?//', 'i')).test(href)) {
    return href;
  }

  // Check if link is a mail-reference (mailto:…)
  if (href.indexOf("mailto:") == 0) {
    return href;
  }

  //Link if href is a simple anchor link
  let anchorText = "";
  const anchorPosition = href.indexOf('#');
  if (anchorPosition === 0) {
    return href;
  }
  if (anchorPosition >= 0) {
    anchorText = href.substring(anchorPosition);
    href = href.substring(0, anchorPosition);
  }

  //Remove md-extension and replace to much back-links
  href = href
    // .replace (fileExtension, '') //Replace file extension
    // .replace (/\.\.\//ig, '') //replace all path back links
    .replace(/\\\ /ig, ' ') //replace all the "\ " in links
    .replace(/\%20/ig, ' '); //replace all the "%20" in links

  // If link starts with "/"-slash, we have a "root" link from where you can navigate to every projects files
  if (href.substring(0, config.rootLinkIdentifier.length) === config.rootLinkIdentifier) {
    const rootPrefix = path.relative(path.dirname(siteObject.filePath), config.symlinkDocumentationPath);
    href = path.join(rootPrefix, href.substring(config.rootLinkIdentifier.length));
  }

  // Check if file exists, if not throw warning but return link
  if (!fs.existsSync(path.resolve(path.dirname(siteObject.filePath), href))) {
    logToConsole("phoenix-BUILDER", "Adjust MD-Links", "Warning: Link wrong or file not found! (" + href + anchorText + ")", true);
  }

  // simply switch extension and return link
  const extensionName = path.extname(path.basename(href));
  return secureFilePath(path.dirname(href) + "/" + path.basename(href, extensionName) + (extensionName === ".md" ? ".html" : extensionName)) + anchorText;
};

const customA = (...props) => {
  const link = adjustLink(props[0].href, props [0].routes, props [0].siteObject);
  const externalLink = (new RegExp('^(?:[a-z]+:)?//', 'i')).test(link);
  const hasExternalIcon = (externalLink ? (!(new RegExp('cake\.schwarz\/', 'i')).test(link)) : false);
  return (
    <a href={link}
       title={props[0].children}
       target={externalLink ? "_blank" : "_self"}
       rel={externalLink ? "noopener" : undefined}
       data-external-icon={hasExternalIcon ? true : undefined}
    >
      {props[0].children}
    </a>
  );
};

const customH1 = (...props) => (
  <h1 className="PhoenixFramework__Headline">
    {props [0].children}
  </h1>
);

// Store the latest headlines to concatenate them in it's anchor links
let latestHeadlines = {
  /*
      h2: "XXX",
      h3: "XXX",
      …
  */
};
const customHeadline = (...props) => {
  // Set the anchorName to the headlines
  latestHeadlines ["h" + props [0].level] = props [0].id;
  const TagName = `h${props [0].level}`;
  let anchorName = "";
  for (let i = 2; i < props [0].level; i++) {
    anchorName += latestHeadlines ["h" + i] + "-";
  }
  anchorName += props [0].id;
  return (
    <TagName>
      <a className="PhoenixFramework__Headline_Hidden_Anchor" name={anchorName}></a>
      <div className="PhoenixFramework__Headline_Content">
        {props [0].children}
        <a className="PhoenixFramework__Headline_Anchor" href={'#' + anchorName}>
          #
        </a>
      </div>
    </TagName>
  );
};

/*
    Checks if a requested Site exists. If so, displays it. If not, displays 404.
 */
const MarkdownPage = ({
                        routes,
                        siteObject,
                      }) => (
  <div className="PhoenixFramework__MainInner">
    <Markdown
      className="phoenix-content"
      children={readMarkupFile(siteObject.filePath)}
      options={{
        namedCodesToUnicode: {
          le: '\u2264',
          ge: '\u2265',
        },
        overrides: Object.assign(
          {
            a: {
              component: customA,
              props: {
                routes,
                siteObject,
              }
            },
            table: {
              component: Table
            },
            h1: {
              component: customH1,
            },
            h2: {
              component: customHeadline,
              props: {
                level: 2,
              },
            },
            h3: {
              component: customHeadline,
              props: {
                level: 3,
              },
            },
            h4: {
              component: customHeadline,
              props: {
                level: 4,
              },
            },
            h5: {
              component: customHeadline,
              props: {
                level: 5,
              },
            },
            h6: {
              component: customHeadline,
              props: {
                level: 6,
              },
            },

            img: {
                component: ShowroomExample,
                props: {
                    siteObject,
                },
            },
            blockquote: {
              props: {
                className: "blockquote PhoenixFramework__blockquote"
              }
            },
            /*
           Iframe: Iframe,*/
           ContentRack: {
               component: ContentRack,
               props: {
                   siteObject,
               }
           }
          },
        )
      }}
    />
  </div>
);

export default MarkdownPage;
