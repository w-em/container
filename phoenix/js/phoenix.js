//Import Polyfills and external dependencies
import svg4everybody from "svg4everybody";

//Import PHOENIX Dependencies as you want (you can simply remove the lines you do not need)
import accordion from "./accordion";
import alert from "./alert";
import form from "./form";
import header from "./header";
import popover from "./popover";
import subnavigation from "./subnavigation";
import tab from "./tab";
import themeSlider from "./themeSlider";
import totop from "./toTop";
import "./cookieAlert";

( () => {
  //Run external dependencies
  svg4everybody ();

  //Scripts to load when document-loaded
  accordion ();
  alert ();
  form ();
  header ();
  popover ();
  subnavigation ();
  tab ();
  themeSlider.initializeAllSliders ();
  themeSlider.initializeAllSliders ({
    test: 'test'
  });
  totop ();
})();
