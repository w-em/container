import React from "react";
import config from "@root/config"

const Header = ( { active, routes } ) => (
    <nav className="navbar PhoenixFramework__Header">
        <div className="PhoenixFramework__Header__Inner">
            <a className="navbar-brand PhoenixFramework__Header__Brand p-0" href={config.rootPath}>
                <img className="PhoenixFramework__HeaderImage" src={config.assetServerPath + "images/wolter-e-marketing-logo-green.png"} />
            </a>

            <div className="PhoenixFramework__HeaderVersionTag nav-item d-flex align-items-end mr-auto">
                <span className="badge badge-primary">v1.0.0</span>
            </div>
        </div>
    </nav>
);
export default Header;
