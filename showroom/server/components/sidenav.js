import React from "react";
import Markdown from 'markdown-to-jsx';

import { getMatchingKey, readMarkupFile } from "../helper";

const createAnchorLinks = (route) => {
  const markdown = readMarkupFile (route);

  //Extract all level 2 headlines for sub navigation
  let lines = markdown.split ('\n');
  let headlines = [];
  for (let line of lines) {
    if (line.match (/^\#\#[^\#]+$/ig)) {
      headlines.push (line.replace (/\#/g, '').trim ());
    }
  }

  //only render submenu, when there are more than one sub-links
  if (headlines.length <= 1) {
    return null;
  }

  return (
    <ul className="PhoenixFramework__SideNav__List PhoenixFramework__SideNav__Section__List__Item__AnchorList">
      {
        headlines.map ((headline, idx) => (
          <li key="headline" className="PhoenixFramework__SideNav__Section__List__Item__AnchorList_Item">
            <a data-controller="cake/sidenav/anchor/link" className="PhoenixFramework__SideNav__Link PhoenixFramework__SideNav__Section__List__Item__AnchorList_Item_Link" href={"#" + headline.replace (/[^\w\s\-]/gi, '').replace (/\s/g, '-').toLowerCase ()}>
              <Markdown
                children={headline}
                options={{
                  // To Remove all links and other stylings from markdown!
                  createElement(menuSection, props, children) {
                    return (<React.Fragment>{children}</React.Fragment>);
                  },
                }}
              />
              {/* <Markdown>{headline}</Markdown> */}
            </a>
          </li>
        ))
      }
    </ul>
  );
}

const Section = ({ siteObject, menuSection, routes = [] }) => {
  //Filter routes that exists in this path
  routes = routes.filter (route => route.path !== null);
  if (routes.length <= 0) {
    return null;
  }

  return (
    <div className="PhoenixFramework__SideNav__Section">
      <h2 className="PhoenixFramework__SideNav__Section__Title">{menuSection}</h2>
      <ul className="PhoenixFramework__SideNav__List PhoenixFramework__SideNav__Section__List">
        {routes.map( route => (
          <li key={ route.key } className="PhoenixFramework__SideNav__Section__List__Item">
            <a className={ `PhoenixFramework__SideNav__Link PhoenixFramework__SideNav__Section__List__Item__Link ${ siteObject.path && siteObject.key === route.key && siteObject.section === route.section ? "active" : "" } ` }
               href={route.urlPath}>
              {route.title}
            </a>

          </li>
        ))}
      </ul>
    </div>
  );
};

const Sidenav = ( { routes, siteObject } ) => (
  <aside className="PhoenixFramework__SideNav">
    <div className="PhoenixFramework__SideNav__Scroll">
      {Object.entries(routes).map( ([key,section],index) => (
        <Section key={index} siteObject={siteObject} menuSection={ key } routes={section} />
      ))}
    </div>
  </aside>
);


export default Sidenav;
