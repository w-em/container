import React from "react";

const Table = ( { children, className="", ...props } ) => (
    <div className={"table-responsive"}>
        <table className={"table " + className} {...props}>
            {children}
        </table>
    </div>
);

export default Table;
