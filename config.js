const packageJson = require ('./package.json');
const path = require ("path");

//This file should not be written using the module syntax of ES6
module.exports = class Configuration {

    static get isDevelopMode () {
        return String (process.env.NODE_ENV).toLowerCase () == "develop";
    }

    static get hiddenFolderPrefixes () {
        return [
            ".",
            "_",
        ];
    }

    static get hiddenExpectionFolders () {
        return [
            "examples",
            "_assets",
        ];
    }

    static get assetPath () {
        return Configuration.basePath + "assets/";
    }

    static get assetServerPath () {
        return Configuration.basePath + "assets-server/";
    }

    static get basePath () {
        return "/";
    }

    static get documentationBuildPath () {
        return "../dist/docs/";
    }

    static get developementServerPort () {
        return process.env.PORT || 2020;
    }

    static get packageJson () {
        return packageJson;
    }

    static get navigationConfigFileName () {
        return "nav.yml";
    }

    static get symlinkDocumentationPath () {
        return path.resolve (__dirname, "./src/_docs");
    }

    static get phoenixPath () {
        return path.resolve (__dirname, "./phoenix");
    }

    static get showroomPath () {
        return path.resolve (__dirname, "./showroom");
    }

    static get rootLinkIdentifier () {
        return "/";
    }

    static setActiveMenuSection (menuSection) {
        global.menuSection = menuSection;
    }

    static get activeMenu () {
        return global.menuSection;
    }
}
