/*
    Page Builder used to generate the different types of html-pages.
 */
import "module-alias/register";
import fs from "fs-extra";
import React from "react";
import config from "@root/config";
import { renderToString } from "react-dom/server";
import Helmet from "react-helmet";
import Layout from "@showroom/server/components/layout";
import MarkdownPage from "@showroom/server/components/MarkdownPage";
import { logToConsole } from "@showroom/server/helper";
import { htmlTemplate, htmlTemplateContent } from "@showroom/server/templates";

const buildPage = (jsx, filePath, template = htmlTemplate) => {
    logToConsole ("SHOWROOM-BUILDER", "Build page", filePath);
    const htmlString = new Uint8Array(
        Buffer.from(
            template (
                renderToString( jsx ),
                Helmet.renderStatic(),
            )
        )
    );
    fs.writeFileSync( filePath, htmlString );
}

/*
    Create index/start page
 */
export const buildContentPage = (routes, outputFolder, ContentPage) => {
    const fileName = outputFolder + ContentPage.filePath;
    buildPage (
        (
            <React.Fragment>
                <Helmet>
                    <title>{ ContentPage.title }</title>
                </Helmet>
                <ContentPage.head />
                <ContentPage.component routes={ routes } />
                <ContentPage.foot />
            </React.Fragment>
        ),
        fileName,
        ContentPage.template ? ContentPage.template : htmlTemplateContent
    );
};

//Build Route
export const buildRoute = (routes, siteObject, outputFile) => {
    config.setActiveMenuSection (siteObject);

    buildPage (
        (
            <React.Fragment>
                <Helmet>
                    <title>{ siteObject.title } | Phoenix</title>
                </Helmet>
                <Layout
                    routes={routes}
                    siteObject={siteObject}
                >
                    <MarkdownPage
                        routes={routes}
                        siteObject={siteObject}
                    />
                </Layout>
            </React.Fragment>
        ),
        outputFile,
    );
};
