import fs from "fs";
import path from "path";
import yaml from "js-yaml"
import config from "@root/config";

const fetchFilesCache = {};

/*
    LOG to console with standardized structure
*/
export const logToConsole = (namespace, action, data = "", highlight = false) => {
  if (highlight) {
    logToConsole (namespace, "-".repeat(action.length));
  }
  console.log (
    "\x1b[33m%s\x1b[0m %s \x1b[35m%s\x1b[0m %s \x1b[32m%s\x1b[0m", //Set Colors for the log
    namespace, "|",
    action, data ? "|" : "",
    data
  );
  if (highlight) {
    logToConsole (namespace, "-".repeat(action.length));
  }
};

/*
    Digs through a given Directory, eg. 'CakePhoenix/Container/Cake',
    gets all Subdirectories, eg. Components, Content
    and looks up all names of all folder inside of them.
    Those are the routes.
 */

export const getRoutes = ( routesPath ) => {
  const routes = {};

  try {
    let navConfig = yaml.safeLoad(fs.readFileSync(path.resolve (routesPath, 'nav.yml'), 'utf8'));

    //Add All the yaml pages to the routes
    for (let main of navConfig) {
      let mainRoutes = [];

      //Add all the pages from nav.yml
      if (Array.isArray (main.pages)) {
        for (let page of main.pages) {

          //Set Page path, if not existing, set default path
          const basePath = routesPath + '/' +  main.path;
          let pagePath = page.path;
          if (!pagePath) {
            pagePath = page.title;
            if (fs.existsSync (basePath + pagePath)) {
              pagePath += '/' + page.title.toLowerCase ();
            } else {
              pagePath = pagePath.toLowerCase ();
            }
          }

          mainRoutes.push({
            title: page.title,
            basePath: getBaseFilePath (basePath + pagePath),
          });
        }
      }

      //Add all the Markdown files automatically
      mainRoutes.push(...getMarkdownFilePaths (routesPath + '/' + main.path));

      //Remove duplicated files + ignores from nav.yml
      mainRoutes = removeDuplicates (mainRoutes, "basePath");

      if (Array.isArray (main.ignores)) {
        for (let page of main.ignores) {
          mainRoutes = mainRoutes.filter (route => route.path !== routesPath + '/' +  main.path + path.basename(page.path, path.extname(page.path)));
        }
      }

      //Extend route informations
      mainRoutes = mainRoutes.map (route => extractSectionInformations (route, main.key));

      routes [main.title.toLowerCase()] = mainRoutes;
    }
  } catch (e) {
    console.error ("Error reading nav.yml file!", e);
  }

  return routes;
};

export const createSiteObject = ( category, filePath, routesPath ) => {
  const title = path.basename (filePath, ".md");
  return {
    category,
    title,
    urlPath: secureFilePath (
      path.relative (
        routesPath,
        path.resolve (
          path.dirname (filePath),
          title + ".html"
        )
      )
    ),
    filePath,
  };
};

export const getRoutes1= ( routesPath ) => {
  const routes = {};

  // Read all the sections
  const navigationConfig = getYmlConfig ( path.resolve (routesPath, config.navigationConfigFileName));

  //5. Add all the nav.yml files to the route
  for (const categoryConfig of navigationConfig) {
    const category = path.basename (categoryConfig.path);
    const categoryRoute = {};

    for (const site of (categoryConfig.pages || [])) {
      const mdfilePath = path.resolve (routesPath, category, site.path);
      categoryRoute [mdfilePath] =  createSiteObject (
        category,
        mdfilePath,
        routesPath
      );
    }
    routes [category] = categoryRoute;
  }

  return routes;
};

export const getYmlConfig = ( ymlPath ) => {
  let ymlConfig = [];
  try {
    if (fs.existsSync (ymlPath)) {
      ymlConfig = yaml.safeLoad(fs.readFileSync(ymlPath, 'utf8'));
      if (!ymlConfig || typeof ymlConfig !== "object") {
        ymlConfig = [];
      }
    }
  } catch (e) {
    logToConsole ("phoenix-BUILDER", "READ-NAV-YML", "Error reading .yml file on path: " + ymlPath, true);
  }
  return ymlConfig;
};

export const getBaseFilePath = (filePath) => {
  let fileName = path.basename(filePath, path.extname(filePath));
  /*
  config.menuSections.forEach (menuSection => {
    if (fileName.substr (fileName.length - menuSection.suffix.length, fileName.length) === menuSection.suffix) {
      fileName = fileName.substr (0, fileName.length - menuSection.suffix.length);
    }
  });*/
  return path.dirname(filePath) + '/' + fileName;
}


export const getMarkdownFilePaths = (folderPath) => {
  let routes = [];

  //Iterate through all folders inside this path and extract .md files
  const menuSections = getAllSubElements(folderPath, true);
  menuSections.forEach(section => {
    const sectionPath = folderPath + section;
    routes.push (...getMarkdownFilePaths ( sectionPath + '/' ));
  });
  //Extract all Markdown-Files from Folder
  const markdownFiles = getAllSubElements (folderPath, false);

  for (let file of markdownFiles) {
    //Only add .md files
    if (file.substr (file.length - 3, 3).toLowerCase () !== ".md")
      continue;

    //Only add raw-file
    routes.push ({
      basePath: getBaseFilePath (folderPath + file),
    });
  }

  return routes;
};

// /*
//     gets all subfolders filtered by given strings.
//  */
export const getAllSubElements = (path, onlyFolders = true) => {
  if (!fs.lstatSync(path).isDirectory ())
    return null;
  const folders = fs.readdirSync( path );
  let subPaths = folders
    .filter( folder => folder.indexOf( "_" ) !== 0 );

  if (onlyFolders)
    subPaths = subPaths.filter( folder => fs.lstatSync(path + '/' + folder).isDirectory ());
  else
    subPaths = subPaths.filter( folder => !fs.lstatSync(path + '/' + folder).isDirectory ());

  //Filter all files with dot (.) as first name (.DS_Store, .gitignore and other config files)
  subPaths = subPaths.filter( folder => folder.indexOf( "." ) !== 0 );
  return subPaths;
};

export const removeDuplicates = (myArr, prop) => {
  return myArr.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
  });
}

export const extractSectionInformations = (route, section) => {

  const key = path.basename(route.basePath);
  const title = route.title ? route.title : beautifySidebarNavName (key);

  let mdPath = route.basePath + '.md';
  let filePath = fs.existsSync (mdPath) ? mdPath : null;

  //Set Links
  let href = config.basePath + encodeURIComponent (section) + '/' + encodeURIComponent (key) + '/' + encodeURIComponent (key) + ".html"

  return {
    section,
    key,
    title,
    urlPath: secureFilePath (href),
    filePath,
  };
}

/*
    Read the markdown-files
*/
export const readMarkupFile = (mdPath) => {
  if (!fs.existsSync(mdPath)) {
    if (path.basename (mdPath) !== config.changeLogExtensionFileIdentifier) {
      logToConsole ("phoenix-BUILDER", "READ-MD-FILE", "No md-file found for " + mdPath, true);
    }
    return "";
  }

  let mdContent = fs.readFileSync(mdPath, 'utf8');
  mdContent = templateString (mdContent);

  return mdContent;
};

export const readChangelogFile = (mdPath, updateHierarchy = true) => {
  let changelogContent = readMarkupFile (mdPath, false);
  if (updateHierarchy) {
    //Increase Level of Headlines by one
    changelogContent = changelogContent.replace(/^\#/igm, "##");
  }
  return changelogContent;
};

/*
    Handle Template-Strings
*/
export const templateString = (str, o) => {
  const regexp = /{{\s*([^\s{}]+)\s*}}/g;
  return str.replace(regexp, (ignore, key) => {
    let value = o;
    const keys = key.split (".");
    for (const k of keys) {
      if (!value [k]) {
        return "";
      }
      value = value [k];
    }
    return value;
  });
};

/*
    Beautify Sidebar Nav-Name
*/
export const beautifySidebarNavName = (string) => {
  //Remove file-extension if available
  string = string.split('.').length > 1 ? string.split('.').slice(0, -1).join('.') : string;

  //Capitalisze first Letter always!
  string = capitalizeFirstLetter (string);

  //Create Space between CamelCase names
  string = string.replace (/([a-z])([A-Z])/g, '$1 $2');
  //Replace dashes (-) with ands (&)
  string = string.replace ('-', ' & ');
  return string;
}

/*
    Capitalizes the first letter of a given string.
 */
export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

/*
    Takes an object and a value and determines the key(name) of the given object:

    eg.:
        testObject = {
            testKey: "hello",
            testedKey: "what?"
        }
        testValue = "what?"

        getMatchingKey(testObject, testValue) returns "testedKey"
 */
export const getMatchingKey = (object, value) => {
  for ( let key in object ) {
    if(object.hasOwnProperty(key)){
      if(object[key] === value) {
        return key;
      }
    }
  }
};

export const getSections = ( routes ) => {
  return Object.keys (routes).filter (key => !config.hiddenFolderPrefixes.includes (key.substr (0, 1)));
}

export const getCategories = ( routes, section ) => {
  return Object.values (routes [section]);
}

export const getSites = ( routes, section, category ) => {
  if (!routes [section] || !routes [section] [category]) {
    return [];
  }
  return Object.values (routes [section] [category]);
}

export const fetchFiles = ( searchPath, onlyDirectories = false, recursively = false, fileTypes = [], hiddenPrefixes = config.hiddenFolderPrefixes) => {
  //Check if searchPath is a file, if so break
  const pathStat = fs.lstatSync(searchPath);
  if (!pathStat.isDirectory () && !pathStat.isSymbolicLink ())
    return [];

  // Read the directory and cache files or otherwise simply use the cached file-paths
  if (!fetchFilesCache [searchPath] || !Array.isArray (fetchFilesCache [searchPath])) {
    const foundPaths = fs.readdirSync (searchPath);
    fetchFilesCache [searchPath] = [];
    foundPaths.forEach (foundPath => {
      const subpathStat = fs.lstatSync(path.resolve (searchPath, foundPath));
      fetchFilesCache [searchPath].push ({
        foundPath,
        isDirectory: (subpathStat.isDirectory () || subpathStat.isSymbolicLink ()),
      });
    });
  }

  // Init the file-cache helper
  let structure = {};

  //Iterate through the found paths
  fetchFilesCache [searchPath].forEach (({ foundPath, isDirectory }) => {
    if ( isDirectory ) {
      //--- Handle Folder ---//

      let folderStructure = {};
      //Add Folders recursively if active
      if (recursively) {
        folderStructure = fetchFiles (
          path.resolve (searchPath, foundPath),
          onlyDirectories,
          recursively,
          fileTypes
        );
      }
      structure [foundPath] = folderStructure;
    } else if (!onlyDirectories) {
      //--- Handle File ---/

      // Check if this folder has "hidden" prefix
      if (hiddenPrefixes.includes (foundPath [0])) {
        return;
      }

      // Check if this file does not have a "search" file-type
      if (fileTypes.length >= 1 && !fileTypes.includes (path.extname (foundPath))) {
        return;
      }

      structure [foundPath] = path.resolve (searchPath, foundPath);
    }
  });
  return structure;
};

export const getFilePathsFromStructure = (structure = {}, excludeFolders = []) => {
  let files = [];
  for (const structureEntry in structure) {
    if (typeof structure [structureEntry] === "string") {
      files.push (structure [structureEntry]);
    } else if (typeof structure [structureEntry] === "object") {
      if (excludeFolders.includes (structureEntry)) {
        continue;
      }
      files = [
        ...files,
        ...getFilePathsFromStructure (structure [structureEntry]),
      ];
    }
  }
  return files;
}


export const secureFilePath = ( path ) => {
  path = path.replace (/\s/g, "_"); //Replace spaces & control characters with underscore (_)
  path = path.replace(/[^a-zA-Z0-9\_\/\.\-\@\+]/g, "");
  return path;
}
