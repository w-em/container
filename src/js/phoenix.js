//Import Polyfills and external dependencies
import svg4everybody from "svg4everybody";

//Import PHOENIX Dependencies as you want (you can simply remove the lines you do not need)
import accordion from "@phoenix/js/accordion";
import alert from "@phoenix/js/alert";
import form from "@phoenix/js/form";
import header from "@phoenix/js/header";
import popover from "@phoenix/js/popover";
import subnavigation from "@phoenix/js/subnavigation";
import tab from "@phoenix/js/tab";
import themeSlider from "@phoenix/js/themeSlider";
import totop from "@phoenix/js/toTop";
import "@phoenix/js/cookieAlert";

( () => {
  //Run external dependencies
  svg4everybody ();

  //Scripts to load when document-loaded
  accordion ();
  alert ();
  form ();
  header ();
  popover ();
  subnavigation ();
  tab ();
  themeSlider.initializeAllSliders ();
  totop ();
})();
