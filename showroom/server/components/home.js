import React from 'react';
import config from "@root/config";

const Home = ( { routes } ) => (
    <React.Fragment>
        <div className="bg-primary">
            <div className="container-responsive home-banner">
                <div className="home-banner-row">
                    <div className="home-banner-intro">
                        <h1 className="mb-0">W-EM Boilerplate.</h1>
                        <div className="mb-2">Version { config.packageJson.version }</div>
                        <p className="lead mb-2 mb-md-4 mb-lg-8">Welcome to our design system and frontend framework for all digital touchpoints at wem.</p>
                    </div>
                    <div className="home-banner-buttons">
                        <a href='/components/accordion/accordion.html' className="btn btn-primary-negative home-develop">I'm a Developer</a>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>
);

export default Home;
