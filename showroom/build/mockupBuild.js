/*
    Mockup Builder
 */
import "module-alias/register";
import fs from "fs-extra";
import path from "path";
import config from "@root/config";
import ContentPages from "@showroom/server/components/ContentPages";
import { getRoutes, logToConsole, secureFilePath, fetchFiles, getFilePathsFromStructure, getYmlConfig, getCompanies, getProducts, getSections, getCategories, getSites } from "@showroom/server/helper";
import { buildContentPage, buildRoute } from "./pageBuild";
import prepareHtmlExample from "./prepareHtmlExample";


(async () => {
    const outputFolder = path.resolve (__dirname, "../", config.documentationBuildPath);
    fs.ensureDirSync(outputFolder);
    const inputFolder = config.symlinkDocumentationPath;

    //Fetch all the routes
    const routes = getRoutes (inputFolder);

    //Build content-pages of CAKE
    for (const ContentPage of ContentPages) {
        buildContentPage (
            routes,
            outputFolder + "/",
            ContentPage
        );
    }

    for (const section of getSections(routes)) {
        const sectionOutputFolder = path.resolve (outputFolder, secureFilePath (section));
        fs.ensureDirSync(sectionOutputFolder);
        const sectionInputFolder = path.resolve (inputFolder, section);

        //Copy all files expect md files to dist folder + prepare and extend example html files (not hidden [. or _])
        const sectionFileStructure = fetchFiles (sectionInputFolder, false, true, [], []);
        const sectionFilePaths = getFilePathsFromStructure (sectionFileStructure);

        const copyPromises = [];

        sectionFilePaths.forEach ((src) => {
            const filePath = path.relative (sectionInputFolder, src);
            const dest = path.resolve (sectionOutputFolder) + path.sep + filePath;
            const secureDest = secureFilePath (dest);

            // Filter all hidden files/folders (_, . prefix)
            if (path.relative (config.symlinkDocumentationPath, src).split (path.sep).find (pathPart => {
                if (config.hiddenFolderPrefixes.includes (pathPart.substr (0, 1)) && !config.hiddenExpectionFolders.includes (pathPart)) {
                    return true;
                }
                return false;
            })) {
                // skip this file
                return;
            }

            // If this file is an html file, prepare it for our showroom
            if (['.md'].includes (path.extname (src))) {
                return;
            }

            // If this file is an html file, prepare it for our showroom
            if (['.twig'].includes (path.extname (src))) {
                let saveTo = dest.replace('.twig', '.html')
                copyPromises.push (
                  prepareHtmlExample (src, saveTo, inputFolder),
                );
                return;
            }

            // If this file is an html file, prepare it for our showroom
            if (['.html', '.htm'].includes (path.extname (src))) {
                copyPromises.push (
                  prepareHtmlExample (src, dest, inputFolder),
                );
                return;
            }

            // Copy all files going through our filters above
            copyPromises.push (
              fs.copy (src, secureDest, { dereference: true }),
            );
        });

        await Promise.all (copyPromises);

        //for (const category of getCategories (routes, section)) {
        //Iterate categories
        for (const siteObject of getCategories (routes, section)) {

            const siteOutputFile = outputFolder + siteObject.urlPath;
            fs.ensureDirSync(path.dirname (siteOutputFile));

            // Build the documentation page
            buildRoute (
              routes,
              siteObject,
              siteOutputFile
            );

        }
    }

    /*
    // Iterate companies
    for (const company of getCompanies (routes)) {

        // Create or clear output folder
        const companyOutputFolder = path.resolve (outputFolder, secureFilePath (company));
        fs.ensureDirSync(companyOutputFolder);
        if (!config.isDevelopMode) {
            fs.emptyDirSync(companyOutputFolder);
        }

        // Iterate products
        for (const product of getProducts (routes, company)) {

            //Create or clear output folder
            const productOutputFolder = path.resolve (companyOutputFolder, secureFilePath (product));
            fs.ensureDirSync(productOutputFolder);

            //Iterate sections
            for (const section of getSections (routes, company, product)) {

                const sectionOutputFolder = path.resolve (productOutputFolder, secureFilePath (section));
                fs.ensureDirSync(sectionOutputFolder);
                const sectionInputFolder = path.resolve (inputFolder, company, product, section);
--
                // Read the project-specific configuration
                const projectConfigurationPath = path.resolve (sectionInputFolder, config.projectConfigFileName);
                const projectConfig = getYmlConfig (projectConfigurationPath);

                //Copy all files expect md files to dist folder + prepare and extend example html files (not hidden [. or _])
                const sectionFileStructure = fetchFiles (sectionInputFolder, false, true, [], []);
                const sectionFilePaths = getFilePathsFromStructure (sectionFileStructure);

                // Use promises for file copy to decrease build-time
                const copyPromises = [];
                sectionFilePaths.forEach ((src) => {
                    const filePath = path.relative (sectionInputFolder, src);
                    const dest = path.resolve (sectionOutputFolder, filePath);
                    const secureDest = secureFilePath (dest);

                    // Filter all hidden files/folders (_, . prefix)
                    if (path.relative (config.symlinkDocumentationPath, src).split (path.sep).find (pathPart => {
                        if (config.hiddenFolderPrefixes.includes (pathPart.substr (0, 1)) && !config.hiddenExpectionFolders.includes (pathPart)) {
                            return true;
                        }
                        return false;
                    })) {
                        // skip this file
                        return;
                    }

                    // Filter showroom-files (product.yml, nav.yml) + user-defined hidden-files (project.yml->hiddenFiles)
                    if ([
                            config.projectConfigFileName,
                            config.projectNavigationConfigFileName,
                            ...(projectConfig.hiddenFiles ? projectConfig.hiddenFiles : []),
                        ].includes (path.relative (sectionInputFolder, src))) {
                        // skip this file
                        return;
                    }

                    // If this file is an html file, prepare it for our showroom
                    if (['.html', '.htm'].includes (path.extname (src))) {
                        copyPromises.push (
                            prepareHtmlExample (src, dest, inputFolder),
                        );
                        return;
                    }

                    // Copy all files going through our filters above
                    copyPromises.push (
                        fs.copy (src, secureDest, { dereference: true }),
                    );
                });

                await Promise.all (copyPromises);

                //Iterate categories
                for (const category of getCategories (routes, company, product, section)) {

                    //Iterate sites
                    for (const siteObject of getSites (routes, company, product, section, category)) {
                        const siteOutputFile = path.resolve (outputFolder, siteObject.urlPath);
                        fs.ensureDirSync(path.dirname (siteOutputFile));

                        // Build the documentation page
                        buildRoute (
                            routes,
                            siteObject,
                            siteOutputFile,
                            projectConfig
                        );
                    }
                }
            }
        }
    }
    */
    logToConsole ("phoenix-BUILDER", "Finished building pages", "", true);
})();
