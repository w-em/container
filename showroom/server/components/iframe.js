import React from "react";

const Iframe = ({
    src,
    title,
    alt,
    ...props
}) => (
    <iframe
        className="phoenix-example-iframe"
        data-controller="theme/example/iframe"
        scrolling="no"
        frameBorder="0"
        width="100%"
        src={src}
        title={title}
        alt={alt}
        {...props}
    >
        {alt}
    </iframe>
);

export default Iframe;
